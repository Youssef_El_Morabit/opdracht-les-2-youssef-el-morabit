﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Fric_frac.Models;
using Microsoft.Extensions.Configuration;

namespace Fric_frac.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // wordt gebruikt in het head->title element 
            // de Master Page
            // test configuration
            ViewBag.Title = "Fric-frac Home";
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult AboutMe()
        {
            ViewData["Message"] = "Korte biografie";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

