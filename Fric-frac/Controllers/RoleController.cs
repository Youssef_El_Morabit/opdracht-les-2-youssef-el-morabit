﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class RoleController : Controller
    {
        private readonly User306Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public RoleController(User306Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Role Index";
            return View(dbContext.Role.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Role Inserting One";
            return View(dbContext.Role.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Role Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
            {
                return NotFound();
            }
            return View(Role);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.FricFrac.Role Role = new Models.FricFrac.Role();
            Role.Name = Request.Form["Role-Name"];

            ViewBag.Message = "Insert een rol in de database";
            dbContext.Role.Add(Role);
            dbContext.SaveChanges();
            return View("Index", dbContext.Role);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Role Role)
        {
            ViewBag.Message = "Insert een rol in de database";
            dbContext.Role.Add(Role);
            dbContext.SaveChanges();
            return View("Index", dbContext.Role);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een rol in de database";
            if (id == null)
            {
                return NotFound();
            }

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
            {
                return NotFound();
            }
            return View(Role);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Role Role)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Role);
                    dbContext.SaveChanges();
                    return View("ReadingOne", Role);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Role.Any(e => e.Id == Role.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", Role);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
            {
                return NotFound();
            }
            dbContext.Role.Remove(Role);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
