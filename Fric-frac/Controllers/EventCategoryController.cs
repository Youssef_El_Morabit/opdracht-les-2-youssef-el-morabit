﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;


namespace FricFrac.Controllers
{
    public class EventCategoryController : Controller
    {

        private readonly User306Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventCategoryController(User306Context dbContext)
        {
            this.dbContext = dbContext;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventCategory Index";
            return View(dbContext.EventCategory.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Inserting One";
            return View(dbContext.EventCategory.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventCategory Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var eventcategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventcategory == null)
            {
                return NotFound();
            }
            return View(eventcategory);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.FricFrac.EventCategory Eventcategory = new Models.FricFrac.EventCategory();
            Eventcategory.Name = Request.Form["EventCategory-Name"];

            ViewBag.Message = "Insert een event categorie in de database";
            dbContext.EventCategory.Add(Eventcategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.EventCategory Eventcategory)
        {
            ViewBag.Message = "Insert een event categorie in de database";
            dbContext.EventCategory.Add(Eventcategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een eventcategorie in de database";
            if (id == null)
            {
                return NotFound();
            }

            var eventcategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventcategory == null)
            {
                return NotFound();
            }
            return View(eventcategory);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventCategory eventcategory)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(eventcategory);
                    dbContext.SaveChanges();
                    return View("ReadingOne", eventcategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Country.Any(e => e.Id == eventcategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", eventcategory);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var eventcategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventcategory == null)
            {
                return NotFound();
            }
            dbContext.EventCategory.Remove(eventcategory);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
