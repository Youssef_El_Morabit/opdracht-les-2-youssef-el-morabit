﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Fric_frac.Controllers
{
    public class BookController : Controller
    {
        public IActionResult Index()
        {
            BookApp.Bll.Book book = new BookApp.Bll.Book();
            dalService.Book = book;
            dalService.ReadAll();
            return View(book);
        }

        private readonly BookApp.Dal.IBook dalService;

        public BookController(BookApp.Dal.IBook dalService)
        {
            this.dalService = dalService;
        }
    }
}