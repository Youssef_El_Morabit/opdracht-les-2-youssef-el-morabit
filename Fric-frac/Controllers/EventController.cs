﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class EventController : Controller
    {

        private readonly User306Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventController(User306Context dbContext)
        {
            this.dbContext = dbContext;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Event Index";
            return View(dbContext.Event.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Event Inserting One";
            ViewBag.EventCategories = dbContext.EventCategory.ToList();
            ViewBag.EventTopics = dbContext.EventTopic.ToList();
            return View();
        }
        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac Event Reading One";
            return View();
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Event Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var _event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (_event == null)
            {
                return NotFound();
            }
            _event.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == _event.EventCategoryId);
            ViewBag.EventCategories = dbContext.EventCategory.ToList();
            _event.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == _event.EventTopicId);
            ViewBag.EventTopics = dbContext.EventTopic.ToList();

            return View(_event);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.FricFrac.Event Event = new Models.FricFrac.Event();
            Event.Name = Request.Form["Event-Name"];

            ViewBag.Message = "Insert een event  in de database";
            dbContext.Event.Add(Event);
            dbContext.SaveChanges();
            return View("Index", dbContext.Event);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Event Event)
        {
            ViewBag.Message = "Insert een event in de database";
            dbContext.Event.Add(Event);
            dbContext.SaveChanges();
            return View("Index", dbContext.Event);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een event in de database";
            if (id == null)
            {
                return NotFound();
            }

            var _event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (_event == null)
            {
                return NotFound();
            }

            _event.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == _event.EventCategoryId);
            _event.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == _event.EventTopicId);
            return View(_event);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Event _event)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(_event);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Event.Any(e => e.Id == _event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.Event);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var _event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (_event == null)
            {
                return NotFound();
            }
            dbContext.Event.Remove(_event);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
