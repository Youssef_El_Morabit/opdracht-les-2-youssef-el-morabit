﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class UserController : Controller
    {

        private readonly User306Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public UserController(User306Context dbContext)
        {
            this.dbContext = dbContext;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac User Index";
            return View(dbContext.User.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac User Inserting One";
            ViewBag.Persons = dbContext.Person.ToList();
            ViewBag.Roles = dbContext.Role.ToList();
            return View();
        }
        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac User Reading One";
            return View();
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac User Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);
            ViewBag.Persons = dbContext.Person.ToList();
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            ViewBag.Roles = dbContext.Role.ToList();

            return View(user);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.FricFrac.User User = new Models.FricFrac.User();
            User.Name = Request.Form["User-Name"];

            ViewBag.Message = "Insert een user in de database";
            dbContext.User.Add(User);
            dbContext.SaveChanges();
            return View("Index", dbContext.User);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.User User)
        {
            ViewBag.Message = "Insert een user in de database";
            dbContext.User.Add(User);
            dbContext.SaveChanges();
            return View("Index", dbContext.User);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een user in de database";
            if (id == null)
            {
                return NotFound();
            }

            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            return View(user);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(user);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.User.Any(e => e.Id == user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.User);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            dbContext.User.Remove(user);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
