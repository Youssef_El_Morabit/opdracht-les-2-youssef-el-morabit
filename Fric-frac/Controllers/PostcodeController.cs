﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Fric_frac.Controllers
{
    public class PostcodeController : Controller
    {
        public IActionResult Index()
        {
            PostcodeApp.Bll.Postcode postcode = new PostcodeApp.Bll.Postcode();
            dalService.Postcode = postcode;
            dalService.ReadAll();
            return View(postcode);
        }
        private readonly PostcodeApp.Dal.IPostcode dalService;

        public PostcodeController(PostcodeApp.Dal.IPostcode dalService)
        {
            this.dalService = dalService;
        }
    }
}