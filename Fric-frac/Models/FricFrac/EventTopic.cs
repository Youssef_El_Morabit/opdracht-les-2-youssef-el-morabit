﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.EntityFrameworkCore.DataAnnotations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class EventTopic
    {
        public EventTopic()
        {
        }

        [Required]
        [StringLength(120)]
        [MySqlCharset("latin1")]
        [FromForm(Name = "EventTopic-Name")]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "EventTopic-Id")]
        public int Id { get; set; }
    }
}
