﻿using System;
using MySql.Data.MySqlClient;
using System.Data;
using System.Reflection;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;


namespace AdoDotNet
{
    class Learning
    {
        public static void TestMySqlConnector()
        {
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
            string ConnectionString = "server= 164.132.231.13; user id=user306;password=GUDHX9QS;port=3306;database=user306;SslMode=none;";
            connection.ConnectionString = ConnectionString;
            Console.WriteLine("Connectie gemaakt.");
            using (connection)
            {
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "select Name, Id from EventTopic;";
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.WriteLine("{0}\t{1}", reader["Name"],
                            reader["Id"]);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();
            }
            Console.ReadKey();
        }


        public static void FricFracDalTest()
        {
            Console.WriteLine(" Fric-frac DAL test");
            FricFrac.Dal.EventTopic dal = new FricFrac.Dal.EventTopic();
            List<FricFrac.Bll.EventTopic> list = dal.ReadAll();
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventTopic item in list)
                Console.WriteLine(" {0} {1}", item.Id, item.Name);
            FricFrac.Bll.EventTopic bll = new FricFrac.Bll.EventTopic();
            bll = dal.ReadOne(4);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            bll = dal.ReadOne(200);
            // we lezen een bestaande categorie in
            bll = dal.ReadOne(6);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we proberen deze gevonden categorie weer toe te voegen
            dal.Create(bll);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we wijzigen de naam van de categorie
            bll.Name = "Hackathon Programmeren";
            // En proberen die toe te voegen
            dal.Create(bll);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we zoeken de naam van de categorie
            bll.Name = "Hackathon Programmeren";
            dal.ReadOne(bll.Id);
            // we wijzigen de naam van de categorie
            bll.Name = "Hackathon Programmeren Deel 2";
            // En proberen die te updaten
            dal.Update(bll);
            Console.WriteLine($" {dal.RowCount} rij(en) gewijzigd, {dal.Message}");
            // nu gaan we de Hackaton categorie deleten (bij mij Id = 23)
            // je moet de Id's nakijken, zeker als je experimenteert met de code
            dal.Delete(23);
            Console.WriteLine($" {dal.RowCount} rij(en) gedeleted, {dal.Message}");
            // zet de naam van categorie met Id = 6 terug op Convention
            // We hebben dit daarnet gewijzigd in Hackathon Programmeren Deel 2
            bll.Id = 6;
            bll.Name = "Convention";
            dal.Update(bll);
            // En toon het nog eens
            list = dal.ReadAll();
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            bll = dal.ReadByName("Auto, Boat & Air");
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventTopic item in list)
                Console.WriteLine(" {0} {1}", item.Id, item.Name);
            //Methode ReadByName testen
            bll = dal.ReadByName("Auto, Boat & Air");
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            //Methode ReadLikeName testen
            bll = dal.ReadLikeName("Family");
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            //Methode ReadLikeXName testen
            bll = dal.ReadLikeXName("Fa");
            Console.WriteLine($" {dal.RowCount} {dal.Message}");

        }

        public static void ReflectPropertiesTryOut()
        {
            // loop through properties using reflection
            // https://msdn.microsoft.com/en-us/library/kyaxdd3x(v=vs.110).aspx
            Type t = typeof(FricFrac.Bll.EventCategory);
            // Get the public properties.
            PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Console.WriteLine("The number of public properties: {0}.\n",
                              propInfos.Length);
            // Display the public properties.
            DisplayPropertyInfo(propInfos);


            // Get the nonpublic properties.
            PropertyInfo[] propInfos1 = t.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);
            Console.WriteLine("The number of non-public properties: {0}.\n",
                              propInfos1.Length);

            // Display all the nonpublic properties.
            DisplayPropertyInfo(propInfos1);
        }

        public static void DisplayPropertyInfo(PropertyInfo[] propInfos)
        {
            // Display information for all properties.
            foreach (var propInfo in propInfos)
            {
                bool readable = propInfo.CanRead;
                bool writable = propInfo.CanWrite;

                Console.WriteLine("   Property name: {0}", propInfo.Name);
                Console.WriteLine("   Property type: {0}", propInfo.PropertyType);
                Console.WriteLine("   Read-Write:    {0}", readable & writable);
                if (readable)
                {
                    MethodInfo getAccessor = propInfo.GetMethod;
                    Console.WriteLine("   Visibility:    {0}",
                                      GetVisibility(getAccessor));
                }
                if (writable)
                {
                    MethodInfo setAccessor = propInfo.SetMethod;
                    Console.WriteLine("   Visibility:    {0}",
                                      GetVisibility(setAccessor));
                }
                Console.WriteLine();
            }
        }

        public static String GetVisibility(MethodInfo accessor)
        {
            if (accessor.IsPublic)
                return "Public";
            else if (accessor.IsPrivate)
                return "Private";
            else if (accessor.IsFamily)
                return "Protected";
            else if (accessor.IsAssembly)
                return "Internal/Friend";
            else
                return "Protected Internal/Friend";
        }

    }
}