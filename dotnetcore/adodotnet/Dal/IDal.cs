﻿using System.Collections.Generic;
namespace FricFrac.Dal
{
    interface IDal<T>
    {
        string Message { get; }
        int RowCount { get; }
        List<T> ReadAll();
        T ReadOne(int id);
        T ReadByName(string name);
        T ReadLikeName(string name);
        T ReadLikeXName(string name);
        int Create(T bll);
        int Update(T bll);
        int Delete(int id);
        
    }
}