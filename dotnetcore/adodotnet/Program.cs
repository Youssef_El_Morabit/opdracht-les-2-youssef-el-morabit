﻿using System;

namespace AdoDotNet
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met ADO.NET in .NET Core!");
            //Learning.TestMySqlConnector();
            //Learning.ReflectPropertiesTryOut();
            Learning.FricFracDalTest();
            Console.ReadKey();
        }
    }
}