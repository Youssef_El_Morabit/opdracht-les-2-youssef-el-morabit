﻿using System;
using System.Collections.Generic;
using System.Text;

namespace learning
{
    class StructVersusClass
    {
        public struct Point
        {
            private int x, y;             

            public Point(int x, int y)   
            {
                this.x = x;
                this.y = y;
            }

            public int X                 
            {
                get { return x; }
                set { x = value; }
            }

            public int Y
            {
                get { return y; }
                set { y = value; }
            }
        }

        class Child
        {
            private int age;
            private string name;

            
            public Child()
            {
                name = "N/A";
            }

            
            public Child(string name, int age)
            {
                this.name = name;
                this.age = age;
            }

            
            public void PrintChild()
            {
                Console.WriteLine("{0}, {1} years old.", name, age);
            }
        }
    }
}
