﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BookApp.Dal
{
    class BookCsv : IBook
    {
        public Bll.Book Book { get; set; }

        public string Message { get; set; }
        private string connectionString = @"Data/Book";
        public string ConnectionString
        {
            get
            {
                return connectionString + ".csv";
            }
            set
            {
                connectionString = value;
            }
        }
        public char Separator { get; set; } = ';';
        public BookCsv(Bll.Book book)
        {
            Book = book;
        }
        public bool ReadAll()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = ConnectionString;
            if (bestand.Lees())
            {
                string[] books = bestand.Text.Split('\n');
                try
                {
                    List<Bll.Book> list = new List<Bll.Book>();
                    foreach (string s in books)
                    {
                        if (s.Length > 0)
                        {
                            list.Add(ToObject(s));
                        }
                    }
                    Book.List = list;
                    Message = $"Het bestand {ConnectionString} is gedeserialiseerd!";
                    return true;
                }
                catch (Exception e)
                {
                    Message = $"Bestand {ConnectionString} is niet gedeserialiseerd. \nFoutmelding {e.Message}";
                    return false;
                }
            }
            else
            {
                Message = $"Bestand {ConnectionString} is niet gedeserialiseerd. \nFoutmelding {bestand.Melding}";
                return false;
            }

        }

        private Bll.Book ToObject(string line)
        {
            Bll.Book book = new Bll.Book();
            string[] values = line.Split(Separator);
            book.Title = values[0];
            book.Year = values[1];
            book.City = values[2];
            book.Publisher = values[3];
            book.Author = values[4];
            book.Edition = values[5];
            book.Translator = values[6];
            book.Comments = values[7];
            return book;
        }

        public bool Create()
        {
            try
            {
                TextWriter writer = new StreamWriter(ConnectionString);
                foreach (Bll.Book item in Book.List)
                {
                    writer.WriteLine("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}",
                        item?.Title,
                        item?.Year,
                        item?.City,
                        item?.Publisher,
                        item?.Author,
                        item?.Edition,
                        item?.Translator,
                        item?.Comments,
                        Separator);
                }
                writer.Close();
                Message = $"Het bestand met de naam {ConnectionString} is gemaakt!";
                return true;
            }
            catch (Exception e)
            {
                Message = $"Bestand met naam {ConnectionString} niet gemaakt.\nFoutmelding {e.Message}.";
                return false;
            }
        }
        public bool Create(char separator)
        {
            Separator = separator;
            return Create();
        }
    }
}
