﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.View
{
    class BookConsole
    {

        public Bll.Book Model { get; set; }

        public BookConsole(Bll.Book book)
        {
            Model = book;
        }

        public void List(Bll.Book book)
        {
            Model = book;
        }

        public void List()
        {
            foreach (Bll.Book item in Model.List)
            {
                Console.WriteLine(item.Title + "\t" + item.Year + "\t" + item.City + "\t" + item.Publisher + "\t" + item.Author + "\t" + item.Edition + "\t" + item.Translator + "\t" + item.Comments);
            }
        }
    }

}
