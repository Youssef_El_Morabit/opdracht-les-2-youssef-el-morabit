﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp
{
    class App
    {
        private readonly Dal.IBook bookDal;
        private readonly AppSettings appSettings;

        public App(Dal.IBook bookDal, IOptions<AppSettings> appSettings) {
            this.bookDal = bookDal;
            this.appSettings = appSettings.Value;
        }

        public void Run() {
            Console.WriteLine("De Book App Generic");
            //read settings
            bookDal.ConnectionString = appSettings.ConnectionString;
            bookDal.ReadAll();
            Console.WriteLine(bookDal.Message);
            View.BookConsole view = new View.BookConsole(bookDal.Book);
            view.List();
            //reserialize
            bookDal.ConnectionString = $"{appSettings.ConnectionString}3";
            bookDal.Create();
            Console.WriteLine(bookDal.Message);
        }
    }
}
