﻿using System;

namespace Wiskune.Meetkunde
{
    class Vormen
    {

        private ConsoleColor kleur;

        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }

        public static string Lijn(int lengte)
        {
            return new string('-', lengte);
        }

    }
}
