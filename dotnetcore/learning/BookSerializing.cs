﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;


namespace DotNetCore.Learning
{
    class BookSerializing
    {

        public static string ReadBooksFromXmlFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"Data/Book.csv";
            bestand.Lees();
            return bestand.Text;
        }

        public static Book[] GetPostcodeArray()
        {
            string[] lines = ReadBooksFromXmlFile().Split('\n');
            Book[] postcodes = new Book[lines.Length];
            int i = 0;
            foreach (string s in lines)
            {
                if (s.Length > 0)
                {
                    postcodes[i++] = BooksCsvToObject(s);
                }
            }
            return postcodes;
        }


        public static List<Book> DeserializeFromXml()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Book[]));
        Book boek = new Book();
        StreamReader file = new StreamReader(@"Data/Book.xml");
        Book[] boeken = (Book[])serializer.Deserialize(file);
        file.Close();
        return new List<Book>(boeken);
    }

        public static List<Book> GetBookList()
        {

            string booksString = ReadBooksFromCSVFile();
            string[] books = ReadBooksFromCSVFile().Split('\n');
            List<Book> list = new List<Book>();
            foreach (string s in books)
            {
                if (s.Length > 0)
                {
                    list.Add(BooksCsvToObject(s));
                }
            }
            return list;
        }
        public static void ShowBooks(List<Book> list)
        {
            foreach (Book boek in list)
            {
                           
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",
                    boek?.Title,
                    boek?.Year,
                    boek?.City,
                    boek?.Publisher,
                    boek?.Author,
                    boek?.Edition,
                    boek?.Translator,
                    boek?.Comment);
            }
        }

        public static string SerializeListToCsv(List<Book> list, string separator)
        {
            string fileName = @"Data/Book.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Book item in list)
                {
                              
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                         item?.Title,
                         item?.Year,
                         item?.City,
                         item?.Publisher,
                         item?.Author,
                         item?.Edition,
                         item?.Translator,
                         item?.Comment);
                }
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Foutmelding.
                
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
            return message;
        }

        public static string ReadBooksFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"Data/Book.csv";
            bestand.Lees();
            return bestand.Text;
        }
        public static List<Book> DeserializeCsvToList()
        {

            string[] postcodes = ReadBooksFromCSVFile().Split('\n');
            List<Book> list = new List<Book>();
            foreach (string s in postcodes)
            {
                if (s.Length > 0)
                {
                    list.Add(BooksCsvToObject(s));
                }
            }
            return list;
        }

        public static Book BooksCsvToObject(string line)
        {
            Book boeken = new Book();
            string[] values = line.Split('|');
            boeken.Title = values[0];
            boeken.Edition = values[1];
            boeken.City = values[2];
            boeken.Comment = values[3];
            boeken.Publisher = values[4];
            boeken.Translator = values[5];
            boeken.Year = values[6];
            boeken.Author = values[7];
            return boeken;
        }

        public static void SerializeListToJson()
        {
            TextWriter writer = new StreamWriter(@"Data/Book.json");
            List<Book> bookList = GetBookList();
            // static method SerilizeObject van Newtonsoft.Json
            string bookString = Newtonsoft.Json.JsonConvert.SerializeObject(bookList);
            writer.WriteLine(bookString);
            writer.Close();
        }

        public static List<Book> DeserializeJsonToList()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"Data/Book.json";
            bestand.Lees();
            List<Book> list = JsonConvert.DeserializeObject<List<Book>>(bestand.Text);
            return list;
        }
    }
}
